import cepPromise from "cep-promise";
import { validateCEPFormat } from "./validation/validateCEP";

export const fetchCEP = (
  cep?: string
): Promise<{
  cep: string;
  uf: string;
  cidade: string;
  rua: string;
  bairro: string;
}> => {
  if (!validateCEPFormat(cep)) return Promise.reject("Formato do CEP inválido");

  return cepPromise(cep).then((response) => ({
    cep: response.cep,
    logradouro: response.street,
    bairro: response.neighborhood,
    municipio: response.city,
    uf: response.state,
  }));
};
