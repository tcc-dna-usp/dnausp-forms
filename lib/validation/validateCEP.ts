import { fetchCEP } from "../fetchCEP";

export const validateCEPFormat = (cep?: string) =>
  !!cep && cep.replace(/\D/, "").replace("_", "").length === 8;
export const validateCEP = async (cep?: string): Promise<string | void> => {
  if (!validateCEPFormat(cep)) return "Formato do CEP inválido";
  const value = await fetchCEP(cep)
    .then(() => undefined)
    .catch((err) => err.message);
  if (value) return value.message;
};
