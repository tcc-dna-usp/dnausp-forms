import validator from "validator";
import { validateCEP } from "./validation/validateCEP";

export class DNAUSPEntryBuilder {
  constructor() {}
}

type Endereco = {
  complemento?: string;
  numero: number;
  cep: string;
};

const validateEndereco = async (endereco: Endereco): Promise<boolean> => {
  if (isNaN(endereco.numero)) return false;
  if (await validateCEP(endereco.cep)) return false;

  return true;
};

type ContatoDNAUSP = {
  email: string;
  nome: string;
  permissoes: string[];
};

const validateContatoDNAUSP = async (
  contato: ContatoDNAUSP
): Promise<boolean> => {
  if (!validator.isEmail(contato.email)) return false;
  if (contato.nome === "") return false;

  return true;
};

type Vinculado = {
  nusp?: string;
  instituto: string;
  tipoDeVinculo?: string;
};

const validateVinculado = async (vinculado: Vinculado): Promise<boolean> => {
  if (
    vinculado.nusp !== "" &&
    vinculado.nusp !== undefined &&
    isNaN(Number(vinculado.nusp))
  )
    return false;
  if (!vinculado.nusp && !vinculado.tipoDeVinculo) return false;

  return true;
};

type Socio = {
  nome: string;
  email: string;
  telefone?: string;
};

const validateSocio = async (socio: Socio): Promise<boolean> => {
  if (socio.nome === "") return false;
  if (!validator.isEmail(socio.email)) return false;
  if (socio.telefone && !validator.isMobilePhone(socio.telefone)) return false;
  return true;
};

type Investimento = {
  tipo: string;
  valor: number;
};

const validateInvestimento = {
  validateCEP,
};

type Faturamento = {
  anoFiscal: number;
  valor: number;
};

type EmpresaInfoBasica = {
  cnpj: string;
  cnae: string;
  tipo: string;
  porte: string;

  nomeFantasia: string;
  razaoSocial: string;
  anoDeFundacao: string;

  descricao: string;
  produtos: string[];
  tecnologias: string[];

  telComercial: string;
  emailInstitucional: string;
  endereco: Endereco;
  site?: string;

  incubadora?: string;

  selo?: boolean;
  contato: ContatoDNAUSP;

  empreendedor: Socio & Vinculado;
  outrosSocios: Array<Socio & Partial<Vinculado>>;

  funcionarios?: number;
  colaboradores?: number;
  estagiariosOuBolsistas?: number;

  investimentos: Investimento[];
  faturamentos: Faturamento[];
};

export class DNAUSPEntry {}
