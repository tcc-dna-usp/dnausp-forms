import React from "react";
import { Story, Meta } from "@storybook/react";
import { withFormik, FormikProps, FormikErrors, Form, Field } from "formik";
import { CNPJInput, CNPJInputProps } from "../components/CNPJInput";
import { validateCNPJ } from "../lib/validation/validateCNPJ";
import { Button } from "carbon-components-react";

export default {
  title: "Inputs/CNPJInput",
  component: CNPJInput,
} as Meta;

const Template: Story<CNPJInputProps> = (args) => <CNPJInput {...args} />;
const TemplateWithValidation: Story<CNPJInputProps> = (args) => (
  <MyForm message="CNPJ" {...args} />
);

export const Primary = Template.bind({});
export const WithValidation = TemplateWithValidation.bind({});

interface FormValues {
  cnpj: string;
}

const InnerForm = (props: FormikProps<FormValues>) => {
  const { touched, errors, isSubmitting } = props;

  return (
    <Form>
      <Field
        type="text"
        name="cnpj"
        invalid={!!errors.cnpj}
        invalidText={errors.cnpj}
        as={CNPJInput}
      />
      <Button
        style={{ marginTop: "16px" }}
        type="submit"
        disabled={isSubmitting}
      >
        Submit
      </Button>
    </Form>
  );
};

const MyForm = withFormik<any, FormValues>({
  mapPropsToValues: () => {
    return {
      cnpj: "",
    };
  },
  validate: (values: FormValues) => {
    let errors: FormikErrors<FormValues> = {};
    if (!validateCNPJ(values.cnpj)) {
      errors.cnpj = "Inválido";
    }
    return errors;
  },
  handleSubmit: console.log,
})(InnerForm);
