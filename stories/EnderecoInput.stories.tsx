import { Story, Meta } from "@storybook/react";
import { Button } from "carbon-components-react";
import { FormikErrors, FormikProps, withFormik } from "formik";
import { useState } from "react";
import validator from "validator";
import {
  EnderecoInput,
  EnderecoInputProps,
  TEndereco,
} from "../components/EnderecoInput";
import { UFList } from "../data/uf";
import { validateCEP } from "../lib/validation/validateCEP";

export default {
  title: "Inputs/EnderecoInput",
  component: EnderecoInput,
} as Meta;

const Template: Story<EnderecoInputProps> = (args) => (
  <EnderecoInput {...args} />
);
const TemplateWithState: Story<EnderecoInputProps> = (args) => {
  const [end, setEnd] = useState<Partial<TEndereco>>({});

  return (
    <>
      <EnderecoInput {...args} value={end} onChange={setEnd} />
      <div style={{ margin: "16px" }}>{JSON.stringify(end, null, 2)}</div>
    </>
  );
};
const TemplateWithFormik: Story<EnderecoInputProps> = (args) => {
  return (
    <>
      <MyForm />
    </>
  );
};

export const Primary = Template.bind({});
export const WithState = TemplateWithState.bind({});
export const WithFormik = TemplateWithFormik.bind({});

interface FormValues extends Partial<TEndereco> {}

const MyForm = withFormik<any, FormValues>({
  mapPropsToValues: () => ({}),
  validate: async (values) => {
    const { bairro, logradouro, municipio, uf } = values;
    const errors: FormikErrors<FormValues> = {};
    console.log(values);

    if (isNaN(Number(values.numero))) errors.numero = "Número inválido";

    const isLogradouroValid = validator.isLength(logradouro || "", {
      min: 1,
      max: 256,
    });
    if (!isLogradouroValid) errors.logradouro = "Logradouro inválido";

    const isMunicipioValid = validator.isLength(municipio || "", {
      min: 1,
      max: 256,
    });
    if (!isMunicipioValid) errors.municipio = "Município inválido";

    const isBairroValid = validator.isLength(bairro || "", {
      min: 1,
      max: 256,
    });
    if (!isBairroValid) errors.bairro = "Bairro inválido";

    if (!uf || !UFList.includes(uf)) errors.uf = "UF inválida";

    await Promise.all([
      validateCEP(values.cep).then((value) => {
        if (value) errors.cep = value;
      }),
    ]);

    return errors;
  },
  handleSubmit: console.log,
})((props: FormikProps<FormValues>) => {
  return (
    <>
      <EnderecoInput
        value={props.values}
        onChange={(values) => {
          Object.entries(values).forEach(([k, v]: [string, any]) => {
            console.log(k, v);
            if (v !== undefined) props.setFieldTouched(k, true);
          });
          props.setValues(values);
        }}
        errors={props.errors}
        touched={props.touched}
      />
      <Button
        onClick={props.handleSubmit}
        kind={props.isValid ? "primary" : "tertiary"}
      >
        Submit
      </Button>
    </>
  );
});
