import { Story, Meta } from "@storybook/react";
import { withFormik, FormikProps, FormikErrors, Form, Field } from "formik";
import { TelefoneInput, TelefoneInputProps } from "../components/TelefoneInput";
import { Button } from "carbon-components-react";
import validator from "validator";

export default {
  title: "Inputs/TelefoneInput",
  component: TelefoneInput,
} as Meta;

const Template: Story<TelefoneInputProps> = (args) => (
  <TelefoneInput {...args} />
);
const TemplateWithValidation: Story<TelefoneInputProps> = (args) => (
  <MyForm message="Telefone" {...args} />
);

export const Primary = Template.bind({});

export const WithValidation = TemplateWithValidation.bind({});

interface FormValues {
  telefone: string;
}

const InnerForm = (props: FormikProps<FormValues>) => {
  const { touched, errors, isSubmitting } = props;

  return (
    <Form>
      <Field
        type="text"
        name="telefone"
        invalid={!!errors.telefone && touched.telefone}
        invalidText={errors.telefone}
        as={TelefoneInput}
      />
      <Button
        style={{ marginTop: "16px" }}
        type="submit"
        disabled={isSubmitting}
      >
        Submit
      </Button>
    </Form>
  );
};

const MyForm = withFormik<any, FormValues>({
  mapPropsToValues: () => {
    return {
      telefone: "",
    };
  },
  validate: (values: FormValues) => {
    let errors: FormikErrors<FormValues> = {};
    if (!validator.isMobilePhone(values.telefone, "pt-BR"))
      errors.telefone = "Telefone inválido";
    return errors;
  },
  handleSubmit: console.log,
})(InnerForm);
