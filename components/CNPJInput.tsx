import { TextInput } from "carbon-components-react";
import { TextInputSharedProps } from "carbon-components-react/lib/components/TextInput/props";
import { ChangeEventHandler, useState } from "react";
import MaskedInput from "react-text-mask";

export type CNPJInputProps = {} & TextInputSharedProps;
export const CNPJInput = (props: CNPJInputProps) => {
  const [cnpj, setCnpj] = useState("");
  const onChange: ChangeEventHandler<HTMLInputElement> = (e) =>
    setCnpj(e.target.value);
  const mask = "99.999.999/9999-99";

  return (
    <MaskedInput
      mask={mask.split("").map((ch) => (ch === "9" ? /[0-9]/ : ch))}
      value={cnpj}
      onChange={onChange}
      {...props}
      render={(ref, props) => (
        <TextInput ref={ref} labelText="CNPJ" id="CNPJ" {...props} />
      )}
    />
  );
};
