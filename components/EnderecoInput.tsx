import { useEffect, useRef, useState } from "react";
import { Button, ComboBox, TextInput } from "carbon-components-react";
import MaskedInput from "react-text-mask";
import { UFList } from "../data/uf";
import { fetchCEP } from "../lib/fetchCEP";

export type TEndereco = {
  complemento: string;
  numero: string;
  cep: string;
  logradouro: string;
  bairro: string;
  municipio: string;
  uf: string;
};

export type TEnderecoErrors = {
  [k in keyof TEndereco]?: string;
};

export type TEnderecoTouched = {
  [k in keyof TEndereco]?: boolean;
};

export type EnderecoInputProps = {
  value?: Partial<TEndereco>;
  onChange?: (value: Partial<TEndereco>) => any;
  errors?: TEnderecoErrors;
  touched?: TEnderecoTouched;
};
export const EnderecoInput = (props: EnderecoInputProps) => {
  const { errors, touched } = props;
  const mask = "99999-999"
    .split("")
    .map((ch) => (ch === "9" ? /[0-9]/ : ch)) as string[];
  const endRef = useRef<MaskedInput>(null);

  const [endereco, setEndereco] = useState<Partial<TEndereco>>({});
  const setStateKey = (key: keyof TEndereco) => (value: string) => {
    setEndereco({ ...endereco, [key]: value });
  };

  useEffect(() => {
    const { value, onChange } = props;
    if (value !== undefined) onChange && onChange(endereco);
  }, [endereco]);

  return (
    <div
      style={{
        maxWidth: "960px",
      }}
    >
      <div style={{ display: "flex" }}>
        <MaskedInput
          mask={mask}
          ref={endRef}
          value={endereco.cep || ""}
          onChange={(evt) => setStateKey("cep")(evt.target.value)}
          render={(ref, props) => (
            <TextInput
              ref={ref}
              labelText="CEP"
              id="cep"
              invalid={!!errors?.cep && !!touched?.cep}
              invalidText={errors?.cep}
              {...props}
            />
          )}
        />
        <Button
          onClick={() =>
            fetchCEP(endereco.cep)
              .then((value) => setEndereco({ ...endereco, ...value }))
              .catch(() => {})
          }
          kind="secondary"
          style={{ marginLeft: "16px" }}
        >
          Validar CEP
        </Button>
      </div>
      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          marginLeft: "-8px",
          marginRight: "-8px",
          marginBottom: "-8px",
        }}
      >
        <div
          style={{
            margin: "8px",
            minWidth: "320px",
            flex: "2",
          }}
        >
          <TextInput
            labelText="Logradouro"
            id="logradouro"
            value={endereco.logradouro || ""}
            onChange={(evt) => setStateKey("logradouro")(evt.target.value)}
            invalid={!!errors?.logradouro && !!touched?.logradouro}
            invalidText={errors?.logradouro}
            maxLength={256}
          />
        </div>
        <div style={{ margin: "8px", minWidth: "192px", flex: "1" }}>
          <TextInput
            labelText="Município"
            id="municipio"
            value={endereco.municipio || ""}
            onChange={(evt) => setStateKey("municipio")(evt.target.value)}
            invalid={!!errors?.municipio && !!touched?.municipio}
            invalidText={errors?.municipio}
            maxLength={256}
          />
        </div>
      </div>
      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          marginLeft: "-8px",
          marginRight: "-8px",
          marginBottom: "-8px",
        }}
      >
        <div
          style={{
            margin: "8px",
            minWidth: "128px",
            flex: "1",
          }}
        >
          <TextInput
            labelText="Bairro"
            id="bairro"
            value={endereco.bairro || ""}
            onChange={(evt) => setStateKey("bairro")(evt.target.value)}
            invalid={!!errors?.bairro && !!touched?.bairro}
            invalidText={errors?.bairro}
            maxLength={256}
          />
        </div>
        <div style={{ margin: "8px", minWidth: "128px", flex: "1" }}>
          <TextInput
            labelText="Complemento"
            id="complemento"
            value={endereco.complemento || ""}
            onChange={(evt) => setStateKey("complemento")(evt.target.value)}
            invalid={!!errors?.complemento && !!touched?.complemento}
            invalidText={errors?.complemento}
            maxLength={256}
          />
        </div>
        <div
          style={{
            margin: "8px",
            minWidth: "64px",
            maxWidth: "128px",
            flex: "1",
          }}
        >
          <TextInput
            labelText="Número"
            id="numero"
            type="number"
            value={endereco.numero || ""}
            onChange={(evt) => setStateKey("numero")(evt.target.value)}
            invalid={!!errors?.numero && !!touched?.numero}
            invalidText={errors?.numero}
            maxLength={256}
          />
        </div>
        <div
          style={{
            margin: "8px",
            minWidth: "144px",
            maxWidth: "192px",
            flex: "1",
          }}
        >
          <ComboBox
            id="uf"
            placeholder="UF"
            value={endereco.uf || ""}
            onChange={(evt) => setStateKey("uf")(evt.selectedItem + "")}
            items={UFList}
            titleText="UF"
            invalid={!!errors?.uf && !!touched?.uf}
            invalidText={errors?.uf}
            maxLength={256}
          />
        </div>
      </div>
    </div>
  );
};
